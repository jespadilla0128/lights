<?php
	Class Lights {

		public $watt_consumption;		
		

		public function get_lights($data)
		{
			switch($data['lights']){
				case "flood":
					$data = $this->flood_lights($data['watts']);
					break;
				case "incandescent":
					$data = $this->incandescent($data['watts']);
					break;
				case "halogen":
					$data = $this->halogen($data['watts']);
					break;
			}
			$data['watt'] = $this->watt_consumption;
			$data['save'] = $data['lights'] - $data['led'];
			return $data;
		}
		function flood_lights($watts)
		{
			//  Flood lights under 100 Watts can be replaced by a 8 Watt non-dimmable LED
			$perYear = $this->watt_per_year($watts);
			$cost = $perYear *.15;
			if($watts < 100 )
			{
				$led = $this->dimmable();
			}elseif ($watts >= 100 ) {
				$led =  $this->nonDimmable();
			}
			return  array('lights' => $cost,'led' => $led);

		}

		
		function incandescent($watts)
		{
			//all incandescent is equal to 8 watt leds (dimmable/non-dimmable)
			$perYear = $this->watt_per_year($watts);
			$data['lights'] = $perYear *.15;
			
			$data['led'] = $this->dimmable();
			$data['led2'] = $this->nonDimmable();
			$data['save2'] = $data['lights'] - $data['led2'];
			
			return $data;
		}

		
		function halogen($watts)
		{
			//halogen  is equal 8 Watt non-dimmable LEDs.
			$perYear = $this->watt_per_year($watts);
			$data['lights'] = $perYear *.15;
			$data['led'] = $this->watts();
			return $data;
		}


		function watt_per_year($watts){
			// get kilo watt per day 
			// assume 3 hrs consumption per day
			$perDay = ($watts * 3) /1000 ;  
			// get per year
			$perYear = $perDay * 365;  //365 days a year
			$this->watt_consumption = $perYear;
			return $perYear;
		}


		protected function dimmable()
		{
			// 8 Watt dimmable LEDs cost $7 each
			// assume 3 hrs consumption per day
			$watt = (3 * 8)/1000;
			$cost = ($watt * 365 ) * .15;
			return $cost + 7; //  LED last at least a year
		}

		protected function nonDimmable()
		{
			// 8 Watt non-dimmable LEDs cost $5 each
			// assume 3 hrs consumption per day
			$watt = (3 * 8)/1000;
			$cost = ($watt * 365 ) * .15;
			return $cost + 5; //  LED last at least a year
		}

		protected function watts()
		{
			// 15 Watt LEDs cost $8.50
			// assume 3 hrs consumption per day
			$watt = (3 * 15)/1000;  //watt per day
			$cost = ($watt * 365) * .15; // cost per year
			return $cost + 8.50;  //  LED last at least a year
		}
	}


	
	
	$light = new Lights();
	if($_POST){
		$data = $light->get_lights($_POST);
		$data['name'] = $_POST['lights'];

		if($data['save'] > 0){
			$text = "<p class='text-success'> You can save <span>$". $data['save'] . "</span>. </p>";
		}else{
			$text = "<p class='text-danger'> LED is much expensive having  additional <span>$". abs($data['save']) . "</span>. </p>";
		}

		if(array_key_exists("save2", $data)){
			if($data['save2'] > 0){
				$text2 = "<p class='text-success'> Using non-dimmable LED can save <span>$". $data['save2'] . "</span>. </p>";
			}
		}
	}
?>