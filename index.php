<html>
<head>
	<title>Calculate Lights Consumption</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
	<?php include("Calculate.php"); ?>
</head>
<body>
	<div class="container">
		<div>
			<form action="" method="Post">
				<select  name="lights" id="lights">
					<option value="flood"> Flood Lights</option>
					<option value="incandescent"> Incandescent Lights</option>
					<option value="halogen"> Halogen Lights</option>
				</select>
				<input type="number" name="watts" id="watts" min="20" value="20" max="500"?>

				<input type="submit" class="btn btn-sm btn-success" value="Submit" ?>
			</form>
		</div>

		<? if($data){ ?>
			<div>
				<p> Watt consumption is <b> <?=$data['watt']?> KWh </b> per year. </p>
				<p> <?=ucfirst($data['name'])?> lights cost per year is <b> <span>$<?=$data['lights']?></span></b>. <p> 
				<p> LED cost per year is <span> $<?=$data['led']?></span>.</p>
				<?=$text?>
				<?if(array_key_exists("led2", $data)){?>
					<p>LED non-dimmable cost per year is <span> $<?=$data['led2']?></span>.</p>
					<?=$text2?>

				<?}?>
				<br>
			</div>

		<?}?>
	</div>

</body>
</html>

